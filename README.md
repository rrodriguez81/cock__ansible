# Cock  Ansible

Proyecto Cock Ansible

## Requerimientos sistema:
 - apt-get install python3-pip
 - pip3 install openstacksdk
 - pip3 install ansible

## Despliegue:
 - ansible-playbook site.yml --extra-vars="os_password=LA_PASSWORD os_instance_state=present" --private-key="LA_CLAVE_PRIVADA"

## Destrucción:
 - ansible-playbook destroy.yml --extra-vars="os_password=LA_PASSWORD os_instance_state=absent"

## Backup:
 - ansible-playbook backup.yml  --extra-vars="os_password=LA_PASSWORD os_instance_state=present" --private-key="LA_CLAVE_PRIVADA"

## Restore:
 - ansible-playbook restore_backup.yml --extra-vars="os_password=LA_PASSWORD os_instance_state=present backup_date=FECHADELBACKUP" --private-key="LA_CLAVE_PRIVADA" --tags "database/files/all"
 
